$(document).ready(function () {

    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) return false;
    });

    jQuery("#btnMSearch").on("click", function (e) {
        searchHandler("tbxMKeyword");
        return false;
    });
    jQuery("#btnHSearch").on("click", function (e) {
        searchHandler("tbxHKeyword");
        return false;
    });

    
    //    if (window.location.pathname.toLowerCase().indexOf("/ja-jp/") >= 0) {
    //        $("#tbxHKeyword,#tbxMKeyword").keydown(function (e) {
    //            if (e.which == 13 || e.keyCode == 13) {
    //                e.preventDefault();
    //                return false;
    //            }
    //        });
    //    }
    //    else {
    $("#tbxMKeyword").keypress(function (e) {
        //e.preventDefault();
        if (e.which == 13)       
                searchHandler("tbxMKeyword");            
        //return false;
    });

    

    $("#tbxHKeyword").keypress(function (e) {  
            //e.preventDefault();
            if (e.which == '13')                
                    searchHandler("tbxHKeyword");
            //return false;                          
        });
  
  
  if (window.location.pathname.toLowerCase().indexOf("/ja-jp/consumer/support/tips") < 0) {
      $("a.ti-nav-logo").attr("href", getSiteUrl() + '/home.aspx');
  }
  
  //hide comunity
  $("li.ti-search-community-desktop").hide();
  $("#liSearchMCommunity").hide();


});

    var enterKeyCountH = 2;
    var enterKeyCountM = 2;
    var enterKeyCountN = 2;
    var enterKeyCount = 2;
    var prodSelected;
    var prodVersion;    
    var tmmsProd = "ウイルスバスター モバイル(Android)";
    var itmmsProd = "ウイルスバスター モバイル(iOS)";
    var jBoxProd = "JewelryBox";
    var ti8Prod = "ウイルスバスター クラウド";
       
    var okaeriVbProd="ウイルスバスター クラウド OKAERI";
    var okaeriVbmProd="ウイルスバスター モバイル OKAERI (Android)";
    var okaeriiVbmProd="ウイルスバスター モバイル OKAERI (iOS)";
    var okaeriPmProd="パスワードマネージャーOKAERI";
    var okaeriJbProd="JewelryBox OKAERI";
    var okaeriForMacProd="ウイルスバスター for Mac OKAERI";
    var okaeriProd="Trend Micro OKAERI(Windows)";
       
       
    var mainProductAlias = '';
    function getSiteUrl() {
//        if (location.href.split("/").length > 6)
//            return location.href.split("/")[6].split(".")[0];
//        else
//            return ""
           var url='';
           var pathArray = window.location.pathname.toLowerCase().split( '/' )


           if (window.location.pathname.toLowerCase().indexOf("/solution/ja-jp/") >= 0 || (window.location.pathname.toLowerCase().indexOf("/docs/") >= 0 && window.location.pathname.toLowerCase().indexOf("/en-us/")==-1 )) //jp solution page Docs
           {           
               url += "/ja-jp/consumer/support/" + pathArray[2];
               if(pathArray.length==6 && window.location.pathname.toLowerCase().indexOf("/docs/") >= 0) {
                 url += "/" + pathArray[3];
               }
                else if (pathArray.length==7 && window.location.pathname.toLowerCase().indexOf("/solution/") >= 0) {
                  url += "/" + pathArray[3];
                }
                 
           }  
       else if (window.location.pathname.toLowerCase().indexOf("/solution/zh-tw/") >= 0 ) //tw solution page 
           {           
               url += "/zh-tw/consumer/support/" + pathArray[2];
                if (pathArray.length==7) {
                  url += "/" + pathArray[3];
                }
                 
           }  
         else if ((pathArray[1].toLowerCase()=="en-us" && pathArray.length < 7) || window.location.pathname.toLowerCase().indexOf("en-us/home/pages/technical-support/docs/") >= 0)  // use en generic search
         {
             url="/en-us/home/pages/technical-support";
         }
           else {
               var j = pathArray.length;
               var i = 0, urlLevel = 5;
               //if (window.location.pathname.toLowerCase().indexOf("helpcenter") >= 0) {
               if (window.location.pathname.toLowerCase().indexOf("ja-jp") >= 0 || window.location.pathname.toLowerCase().indexOf("zh-tw") >= 0 ) {
                   urlLevel = 4;
                 if (pathArray[4]=='okaeri') {
                   if(pathArray[5]=='vb' || pathArray[5]=='vbm'  || pathArray[5]=='ivbm'  || pathArray[5]=='pm' || pathArray[5]=='jb' || pathArray[5]=='pc-cillin' ) {
                     urlLevel =5;
                   }
                 }
                 
               }
               for (i = 1; i < j; i++) {
                   url += "/" + pathArray[i];
                   if (i == urlLevel)
                       break;
               }
           }          
           return url;
    }
    function searchHandler(tbxId) {
        var _q = jQuery.trim(jQuery("#" + tbxId).val().replace(/</g, " ").replace(/>/g, " "));
        var _searchPath = getSiteUrl();
        if (_q == "") {
            alert("Please enter keyword to search");
            jQuery("#" + tbxId).val("");
        }
        else {        
            if (prodSelected) {
        switch (prodSelected) {
          case tmmsProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/vbm/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/vbm/searchresult.aspx?q=" + encodeURIComponent(_q);
            }
            break; 
          case itmmsProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/ivbm/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/ivbm/searchresult.aspx?q=" + encodeURIComponent(_q);
            }
            break; 
          case jBoxProd:
              window.location = "/ja-jp/consumer/helpcenter/trend-micro-jewelrybox-2-x/searchresult.aspx?q=" + encodeURIComponent(_q);
            break;
          case ti8Prod:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/vb/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/vb/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriVbProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/vb/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/vb/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriVbmProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/vbm/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/vbm/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriiVbmProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/ivbm/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/ivbm/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriPmProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/pm/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/pm/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriJbProd:
            if(locale=="zh-tw") {
              if (prodVersion) {
                window.location = "/zh-tw/consumer/support/okaeri/jb/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
              }
              else {
                window.location = "/zh-tw/consumer/support/okaeri/jb/searchresult.aspx?q=" + encodeURIComponent(_q);
              } 
              
            }
            else {
              if (prodVersion) {
                window.location = "/ja-jp/consumer/support/okaeri/jb/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
              }
              else {
                window.location = "/ja-jp/consumer/support/okaeri/jb/searchresult.aspx?q=" + encodeURIComponent(_q);
              } 
            }
            
            break;
          case okaeriForMacProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/vb/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/vb/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;
          case okaeriProd:
            if (prodVersion) {
              window.location = "/ja-jp/consumer/support/okaeri/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = "/ja-jp/consumer/support/okaeri/searchresult.aspx?q=" + encodeURIComponent(_q);
            } 
            break;              
          default: 
            if (prodVersion) {
              window.location = _searchPath + "/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected) + '&v=' + encodeURIComponent(prodVersion);
            }
            else {
              window.location = _searchPath + "/searchresult.aspx?q=" + encodeURIComponent(_q) + '&p=' + encodeURIComponent(prodSelected);
            }
            break;
        }}        
            else {
                window.location = _searchPath + "/searchresult.aspx?q=" + encodeURIComponent(_q);
            }
        }
    }
     
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
      
    $(function() {
   // for video tutorial
               $('a.lightbox-videoguides').click(function(){
                 var hrefVal = $(this).attr('href');
                 var wndwHght = $(window).height();
                 var totalwH = wndwHght - 540;
               $('iframe.modal-dialog-box').attr('src',hrefVal);
               $('#modalVideoTutorial .modal-dialog-box').attr('style','height:'+totalwH+'px;');
               $('#modalVideoTutorial').modal({backdrop: 'static'});
               $('#modalVideoTutorial').modal('show');
               return false;
               });
    
     $('#accExpandAll').click(function(){
       var accClpClass = $('.acc a').hasClass('collapsed');
       if(accClpClass==true){
         $('.acc a').removeClass('collapsed');
         $('.acc-content').addClass('in').attr('style','');
       }
           //$('.acc-content').addClass('in').attr('style','');
       $(this).addClass('hidden');
       $('#accCloseAll').removeClass('hidden');
     });
     
     $('#accCloseAll').click(function(){
       $('.acc a').addClass('collapsed');
       $('.acc-content').removeClass('in');
       // $('.acc-content').removeClass('in');
       $(this).addClass('hidden');
       $('#accExpandAll').removeClass('hidden');
     });
       
       //activate dropdown menu #to exlist > ul 
       $("#exList > ul").addClass("dropdown-menu");
       
       //hide div that is empty
       $('div:empty').hide();
       
       //add hover functionality to top menu
       $('.nav li.dropdown').hover(function() {
         $(this).addClass('open');
       }, function() {
         $(this).removeClass('open');
       });
     

  //jp accordion
    $(function() {
    jQuery(".accordionTitle").click(function(){
    if(jQuery("+.acrdCont",this).css("display")=="none"){
      jQuery(".acrdCont").slideUp("normal");
      jQuery("+.acrdCont",this).slideDown("normal");
      jQuery(".accordionTitle").removeClass("selected");
      jQuery(this).addClass("selected");
    } else {
      jQuery("+.acrdCont",this).slideUp("normal");
      jQuery(this).removeClass("selected");
    }
  });

  jQuery("span.expandAll").click(function () {
   if (jQuery(this).html() == "詳細すべて確認") {
    jQuery(".acrdCont").slideDown("normal");
    jQuery(".accordionTitle").addClass("selected");
    jQuery(".acrdCont").addClass("current");
    jQuery(this).html("詳細すべて閉じる");
   } else {
      jQuery(".acrdCont").slideUp("normal");
    jQuery(".accordionTitle").removeClass("selected");
    jQuery(".pane").removeClass("current");
    jQuery(this).html("詳細すべて確認");
   }
  });
   });  




       /*---- Scroll Navigation ----*/
       var lastScrollTop = 0;
        
       $(window).scroll(function(event){
            var st = $(this).scrollTop();
            if (st > lastScrollTop){

                $('.ti-global-desktop-links').hide();
                $('.ti-name').hide();
                $('.ti-fixed-top-brand').show();
              $("#stickyTitle").attr('style','display:inline');
                    $("#stickyTitle").attr('style','display:inline');
                $('.ti-nav-logo-desktop').hide();
                $('.navbar').addClass("ieDrop");

                     
                var mq = window.matchMedia( "(min-width: 768px)" );

               if(mq.matches) {
                    // the width of browser is more then 767px            
                    $('.navbar').css({
                      '-webkit-box-shadow':'0 1px 4px #ccc',
                      'box-shadow':'0 1px 4px #ccc',
                      'padding-bottom': '15px'
                    });
               } 

            } else if(st == 0) {
                $('.navbar').removeClass("ieDrop");
                $('.ti-global-desktop-links').show()
                $('.ti-name').show();
                $('.ti-nav-logo-desktop').show();
                $('.ti-fixed-top-brand ').hide();
                $("#stickyTitle").hide();
                $('.navbar').css({
                    '-webkit-box-shadow':'none',
                    'box-shadow':'none',
                    'padding-bottom': '0'
   
                });
            }
            lastScrollTop = st;
        });
        /*---- End Scroll Navigation ----*/ 

        //Showing and Hiding Trend Micro Global Menu 
        $('.tm-global-menu-toggle').click(function() {
            $('.ti-global-nav').animate({width:'toggle'},320);
        });
        
        //show search box in navigation 
        $('.ti-search-show').click(function() {
            $('.ti-search-desktop').css({display:'block'}).animate({right:'0'}, 500);
        });
        
        //hide search box in navigation
        $('.ti-search-hide').click(function() {
            $('.ti-search-desktop').animate({right:'-372px'}, 500, function(){
               $(this).css({display:'none'});
            });
        });
     
         });

   // back to top added by myn
   jQuery(document).ready(function() {
        var offset = 220;
        var duration = 500;
        jQuery(window).scroll(function() {
          if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
          } else {
            jQuery('.back-to-top').fadeOut(duration);
          }
        });
        
        jQuery('.back-to-top').click(function(event) {
          event.preventDefault();
          jQuery('html, body').animate({scrollTop: 0}, duration);
          return false;
        })
      });

   // hide video div when there's none
  $(document).ready(function(){
        var vboxsrc = $('param[name="movie"]').attr("value");
        if (vboxsrc == "") {
          $("#vbox").attr("style", "display:none;");
          $("#video").attr("style", "display:none;");
          $("#video-tutorial").attr("style", "display:none;");
          $("#vbtn").attr("style", "display:none;");
        }
        else {
          $("#vbtn").show();
          $("#vbox").attr("style", "display:inline; width:100%; max-width:780px; height:100%; max-height:470px;");
          $("#video").attr("style", "display:inline; width:100%; max-width:780px; height:470px;");
        }
    });

  //hide solution Related Info if there's none
  $(document).ready(function(){
    if($.trim($("#RelatedInfo").val()) == 0 ){
          $("#related").attr("style", "display:none;");
      }
       
     });  

    
 $(document).ready(function(){
    $(".bcb").closest('div[class^="row content-block"]').css({ "border-top": "none", "padding-top": "0px" });                         
    
});
     
$(document).ready(function(){
    var h2 = $("h2.ti-name");
     if($(h2).text() == ""){
      $(h2).html("Home & Home Office Support");$(".ti-mobile-search").attr('style','display:none!important;');$("#mainDesktop").attr('style','margin-top:-44px;');$("#mainTablet").attr('style','margin-bottom:-110px;');$("#mainMobile").attr('style','margin-top:50px; margin-bottom:35px;');$("#stickyTitle").html("Home & Home Office Support");$("#stickyTitle").addClass("ti-sticky-title");$('.ti-fixed-top-brand').attr('style','position:initial;');$('.navbar').attr('style','min-height:70px;');$('.ti-main-menu').attr('style','margin-top: 0!important;');$("#searchH").attr('style','display:none!important;')
     }
    });   
   
//Enchance Nav 
 $(document).ready(function(){
     var pageURL =  $(location).attr('pathname');  // index.php
     var lastURL= pageURL.split('/').pop();
   
     if(lastURL == "home.aspx"){
       $("#newnav1").show();
        $("#newnav2").hide();
       $("#caretNav").hide();
       }
     else {
          $("#newnav2").show();
       $("#caretNav").show();
       $("#newnav1").hide();
       
        }
    });   
         
       
